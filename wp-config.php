<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'fct-v2' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '1234' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '#Zmv5)DSg#H^<{+=xdZ3u&BYS@*}kE-zfD.oa^PGy9u6r6TSxJ1~zEe7(x?[yh#e' );
define( 'SECURE_AUTH_KEY',  'DNU#R;mKd^|V~YlG#)L~ho3$?dS}~_2!ff`?z:N%A:NBwIR?Z4e[9g{;= -e~SW)' );
define( 'LOGGED_IN_KEY',    'd?pg?xI}!MBu}F9n#3?Po+;Nb_=i-Yh*}PxnC>:> )UOxQSCk.x:;CK(M>djECKz' );
define( 'NONCE_KEY',        '6+b)vvML>JUHNNrEosIp2{Ac_u/$=-Aliz}zCww-Ko.;,Q^S2*8dCPZ$Y4diVV!S' );
define( 'AUTH_SALT',        'hD7o%U3=amID!q:]y`[2_$Gakak{M2D%R6Gd>[H1=DB}/VDzL~!l:rZPxDG=Y-Z=' );
define( 'SECURE_AUTH_SALT', 'N~hG9,*XZ#azHVd!`b<QO!s/z/*)zdZ?<f9J1-aNsn7&?-O5#bYBGpGFRZt4p(/-' );
define( 'LOGGED_IN_SALT',   '^dH@[9j7|3)fP-Yu)se-*N.LK^*Yqu/6J*cG0I^f5{Iii2~LS0t`,ZoPP|)/%[]D' );
define( 'NONCE_SALT',       'I^JaA1$<v77W_0 AVF(y`(d;fo3W_]U@|y{B@~p=/Z-zj4tL``+eQO$/24^uNPQ^' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
